Creating an SSH Key
https://docs.gitlab.com/ee/user/ssh.html → Generate an SSH key pair

Open a terminal then type:
ssh-keygen -t ed25519 -C "<comment>" 
or
ssh-keygen -t rsa -b 2048 -C "<comment>"



Copying an SSH Key
C:\Users\ayson\.ssh → open and copy the key in “id_ed25519.pub” file
C:\Users\ayson\.ssh → open and copy the key in “id_rsa.pub” file

Linux:
Terminal → xclip -sel clip < ~/.ssh/id_ed25519.pub
Terminal → xclip -sel clip < ~/.ssh/id_rsa.pub

Mac:
Terminal → pbcopy < ~/.ssh/id_ed25519.pub
Terminal → pbcopy < ~/.ssh/id_rsa.pub

Windows:
Terminal → cat ~/.ssh/id_ed25519.pub | clip
Terminal → cat ~/.ssh/id_rsa.pub | clip



Adding the generated SSH key to git
Gitlab → Edit Profile → SSH Keys 
Paste the key
Enter a title, usage type (optional) and remove the expiration date 
Click the Add key button



Configuring the git account in the device/project
Configure the global user email:
Terminal → git config --global user.email "email"

Configure the global user name:
Terminal → git config --global user.name "username"

Configure project email:
Terminal → git config user.email email"

Configure project username:
Terminal → git config user.name "username"

To check the git user credentials
Terminal → git config --global --list



Git | Local Repository to Remote Repository

1. to INITIALIZE git in a folder:
Terminal → git init

2. to ADD ALL FILES in folder to STAGING area:
Terminal → git add .

3. to COMMIT staged files for pushing:
Terminal → git commit -m "descriptive commit message here"

4. to ADD SSH or HTTPS LINK from GITLAB (IF FIRST TIME ONLY):
Terminal → git remote add origin <insert link here>

5. to PUSH COMMITTED FILES TO GITLAB:
Terminal → git push [remote-name] master 
Terminal → git push origin master 



To check the states of the files/folders
Terminal → git status

Check the commit history
Terminal → git log
Terminal → git log --oneline

Check the remote names and their corresponding urls.
Terminal → git remote -v

Remove a remote repository
Terminal → git remote remove [remote-name]
Terminal → git remote remove origin

dj